//
//  ArrayExtensions.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/11/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}
