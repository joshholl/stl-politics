import Foundation

extension Optional where Wrapped == String {
    var isNilOrEmpty: Bool {
        return self?.isEmpty ?? true
    }
    var isNotNilOrEmpty: Bool {
        return !self.isNilOrEmpty
    }
}
