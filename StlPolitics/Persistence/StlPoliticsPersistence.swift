import Foundation
import CoreData

class StlPoliticsPersistence {
    let container: NSPersistentContainer
    
    init(container: NSPersistentContainer) {
        self.container = container
    }
    
    func delete(withId id: UUID) {
        let context = container.context
        let request: NSFetchRequest<CDPerson> = CDPerson.fetchByUUIDRequest(id: id )
        
        do {
            let results = try context.fetch(request)
            
            if let found = results.first {
                do {
                    context.delete( found)
                    try context.save()
                    
                } catch {
                    print("Failed to save new reminder: \(error)")
                    
                }
            }
        } catch {
            print("Unable to find first reminder: \(error)")
            
        }
        
    }
    
    func fetchAll() -> [Person] {
        let context = container.context
        let request: NSFetchRequest<CDPerson> = CDPerson.fetchRequest()
        
        do {
            let results = try context.fetch(request)
            return results.map{Person.from(data:$0)}
        } catch {
            return []
        }
    }
    
    func upsert(person: Person) -> Bool {
        let context = container.context
        let request: NSFetchRequest<CDPerson> = CDPerson.fetchByProperties(fullName: person.fullName, company: person.company, position: person.position)
        
        do {
            let results = try context.fetch(request)
            
            if let found = results.first {
                found.id = person.id;
                found.fullName = person.fullName
                found.company = person.company
                found.position = person.position
            } else {
                let _ = CDPerson(with: person, intoContext: context)
            }
            
            do {
                try context.save()
            } catch {
                print("Failed to save new person: \(error)")
                return false
            }
        } catch {
            print("Unable to find first person: \(error)")
            return false
        }
        return true
    }
}
