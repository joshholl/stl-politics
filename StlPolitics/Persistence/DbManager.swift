import Foundation
import CoreData

class DbManager {
    let container: NSPersistentContainer;
    init() {
        self.container = DbManager.initializeContainer();
    }
    
    private class func initializeContainer() -> NSPersistentContainer {
        let container = NSPersistentContainer(name: "StlPolitics")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        return container
    }
}
