import Foundation
import CoreData

@objc(CDPerson)
public class CDPerson : NSManagedObject {
    let entityName = "CDPerson"
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    init(with person: Person, intoContext context: NSManagedObjectContext?) {
        super.init(entity: CDPerson.entity(), insertInto: context);
        self.id = person.id
        self.position = person.position
        self.company = person.company
        self.fullName = person.fullName
        self.state = person.state
        self.city = person.city
    }
}
