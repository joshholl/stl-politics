import Foundation
import CoreData

extension CDPerson {
    @nonobjc public class func name() -> String {
        return "CDPerson"
    }
}

extension CDPerson {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDPerson> {
        return NSFetchRequest<CDPerson>(entityName: self.name())
    }
    
    @nonobjc public class func fetchByUUIDRequest(id: UUID) -> NSFetchRequest<CDPerson> {
        let fetch = NSFetchRequest<CDPerson>(entityName: self.name())
        fetch.predicate = NSPredicate(format: "id == %@", id.uuidString)
        
        return fetch
    }
    
    @nonobjc public class func fetchByProperties(fullName: String, company: String, position: String) -> NSFetchRequest<CDPerson> {
        let fetch = NSFetchRequest<CDPerson>(entityName: self.name())
        let fullNamePredicate = NSPredicate(format: "fullName == %@" , fullName)
        let companyNamePredicate = NSPredicate(format: "company == %@" , company)
        let positionPredicate = NSPredicate(format: "position == %@" , position)

        fetch.predicate = NSCompoundPredicate.init(type: .and, subpredicates: [fullNamePredicate, companyNamePredicate, positionPredicate])

        return fetch
    }

    @NSManaged public var id: UUID
    @NSManaged public var position: String
    @NSManaged public var company: String
    @NSManaged public var fullName: String
    @NSManaged public var state: String
    @NSManaged public var city: String
}
