import Foundation
import CoreData

extension NSPersistentContainer {
    
    var context: NSManagedObjectContext {
        if Thread.current.isMainThread {
            return viewContext
        }
        return newBackgroundContext()
    }
}
