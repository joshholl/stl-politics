import UIKit

class PersonsViewController: UITableViewController {
    var model: PersonsModel = PersonsModel()
    var spinner = SpinnerUtility()
    
     override func viewWillAppear(_ animated: Bool) {
        spinner.showActivityIndicator(uiView: self.view)
        self.tableView.reloadData()
        spinner.hideActivityIndicator(uiView: self.view)
     }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfPersons
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let person = model.personAt(index: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonsCell", for: indexPath)
        
        cell.textLabel?.text = person.fullName
        cell.detailTextLabel?.text = person.company
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") {_,_, completionHandler in
            let person = self.model.personAt(index: indexPath.row)
            
            
            let alertController = UIAlertController(title: "Are you sure you want to delete \(person.fullName)", message: nil, preferredStyle: .actionSheet)
            
            
            let deleteAction = UIAlertAction(title: "delete", style: .destructive) { (_) in
                self.model.deletePerson(person: person)
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.endUpdates()
                completionHandler(true)
            }
            
            
            
            let cancelAction = UIAlertAction(title: "cancel", style: .cancel) { (_) in
                completionHandler(false)
            }
            
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let contributionController = segue.destination as? ContributionListViewController {
            guard let cell = sender as? UITableViewCell else { return }
            guard let path = self.tableView.indexPath(for: cell) else { return }
            contributionController.setupForPerson(person: model.persons[path.row])
        }
    }
}
