//import Foundation

typealias ContributionsFetchedCallback = () -> ()

class PersonsModel {
    let session = AppSession.instance
    var contributions: [IndividualContributor] = []
    
    var persons: [Person] {
        return session.persistence.fetchAll()
    }
    
   
    var numberOfPersons: Int {
        return persons.count
    }
    
    func personAt(index: Int) -> Person {
        return persons[index]
    }
    
    func deletePerson(person: Person) {
        session.persistence.delete(withId: person.id)
    }
    
    func fetchContributionsForPerson(person: Person, contributionsFetched: @escaping ContributionsFetchedCallback) {
        session.api.searchForBusinessPerson(name: person.fullName, state: person.state, city: person.city, employer: person.company) { (SearchResult) in
            switch SearchResult {
            case .failure(let error):
                print(error)
                self.contributions = []
            case .success(let data): self.contributions = data
                
                
            }
            contributionsFetched()
        }
    }
}
