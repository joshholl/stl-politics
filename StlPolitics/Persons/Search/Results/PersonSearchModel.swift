import Foundation

class PersonSearchModel {
    let session = AppSession.instance

    var name: String?
    var city: String?
    var state: String?
    var employer: String?
   
    
    var searchResults: [Person] = []
    
    func search(delegate: DataChangedDelegate) {
        session.api.searchForBusinessPerson(name: name, state: state, city: city, employer: employer) { [weak self] result in
            switch result {
            case .success(let contributors):
                self?.searchResults = contributors.map{ Person.from(contributor: $0)}
                self?.searchResults.removeDuplicates()
                delegate.onDataChanged()
            case .failure(let error): print(error)
            }
        }
    }
    
    func savePerson(at index: Int) -> Bool {
        return self.session.persistence.upsert(person: searchResults[index])
    }
    
    func calculateHeightForHeader() -> Int {
        return ([name, city, state, employer].filter { $0.isNotNilOrEmpty}.count * 30) + 20
    }
    
}
