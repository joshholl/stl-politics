import UIKit

typealias SaveCallback = () ->()

class PersonSearchResultsViewController: UIViewController {
 
    @IBOutlet weak var searchResultTableView: UITableView!
 
    var model = PersonSearchModel()
    var spinner: SpinnerUtility!
    
    
    func setCriteria(name: String?, city: String?, state: String?, employer: String?) {
        model.city = city
        model.name = name
        model.employer = employer
        model.state = state
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        spinner.showActivityIndicator(uiView: self.view)
        model.search(delegate: self)
    }
    
    override func viewDidLoad() {
        self.searchResultTableView.delegate = self
        self.searchResultTableView.dataSource = self
        self.spinner = SpinnerUtility()
    }
    
    func afterSave() {
     
    }
}

extension PersonSearchResultsViewController : DataChangedDelegate {
    func onDataChanged() {
        DispatchQueue.main.async { [weak self] in
            guard let instance = self else {
                return
            }
            instance.searchResultTableView.reloadData()
            instance.spinner.hideActivityIndicator(uiView: instance.view)
        }
    }
}

extension PersonSearchResultsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: CGFloat(model.calculateHeightForHeader()))
        
        
        return SearchCriteriaHeaderView(rect: headerFrame, name: model.name, city: model.city, state: model.state, employer:  model.employer)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(model.calculateHeightForHeader())
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result = model.searchResults[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath)
        
        cell.textLabel?.text = result.fullName
        cell.detailTextLabel?.text = result.company
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.model.savePerson(at: indexPath.row) {
            navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension PersonSearchResultsViewController: UITableViewDelegate {
    //this is needed or the view for header in section is not public, but the method itself is part of
    //UITableView ataSource
}
