//
//  SearchCriteriaHeaderView.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/11/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation
import UIKit

class SearchCriteriaHeaderView: UIView {
    let name: String?
    let city: String?
    let state: String?
    let employer: String?
    
    init(rect: CGRect, name: String?, city: String?, state: String?, employer: String?) {
        self.name = name
        self.city = city
        self.state = state
        self.employer = employer
        super.init(frame: rect)
        self.backgroundColor = UIColor.darkGray.withAlphaComponent(CGFloat(0.1))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        let mainLabel = UILabel()
        mainLabel.text = "Result(s) For Criteria";
        mainLabel.textColor = UIColor.black
        mainLabel.font = UIFont.boldSystemFont(ofSize: 20)
        
        mainLabel.frame = CGRect(x: 10, y: 10, width: frame.width, height: 20)
        self.addSubview(mainLabel)
        
        let subLabels = [
            "Name": name,
            "City": city,
            "State": state,
            "Employer": employer
        ]
        
        var childrenAdded = 1
        
        subLabels.forEach{ (key, body) in
            if body.isNotNilOrEmpty {
                let view = buildCriterionLabel(field: key, value: body)
                let yVal = 10 + (childrenAdded * 20)
                view.frame = CGRect(x: 10, y: CGFloat(yVal), width: frame.width, height: 20)
                self.addSubview(view)
                childrenAdded += 1
            }
        }
    }

    
    
    func buildCriterionLabel(field:String, value: String?) -> UIView {
        let label = UILabel()
        label.text = "\(field): \(value ?? "n/a")"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: CGFloat(17))
        
        return label
    }
    
}
