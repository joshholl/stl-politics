//
//  States.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/11/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation


class States {
   
    var values: [State] = []
    
    // ideally the api key wouldn't be hard coded here
    // but since the open fec api only takes it in the get request, and is easily viewable in
    // the browser when using the official site, api key security isn't the biggest concern
    static let all = States().values
    
    init() {
        self.values = [
        State("AK","Alaska"),
        State("AL" , "Alabama"),
        State("AR" , "Arkansas"),
        State("AS" , "American Samoa"),
        State("AZ" , "Arizona"),
        State("CA" , "California"),
        State("CO" , "Colorado"),
        State("CT" , "Connecticut"),
        State("DC" , "District of Columbia"),
        State("DE" , "Delaware"),
        State("FL" , "Florida"),
        State("GA" , "Georgia"),
        State("GU" , "Guam"),
        State("HI" , "Hawaii"),
        State("IA" , "Iowa"),
        State("ID" , "Idaho"),
        State("IL" , "Illinois"),
        State("IN" , "Indiana"),
        State("KS" , "Kansas"),
        State("KY" , "Kentucky"),
        State("LA" , "Louisiana"),
        State("MA" , "Massachusetts"),
        State("MD" , "Maryland"),
        State("ME" , "Maine"),
        State("MI" , "Michigan"),
        State("MN" , "Minnesota"),
        State("MO" , "Missouri"),
        State("MS" , "Mississippi"),
        State("MT" , "Montana"),
        State("NC" , "North Carolina"),
        State("ND" , " North Dakota"),
        State("NE" , "Nebraska"),
        State("NH" , "New Hampshire"),
        State("NJ" , "New Jersey"),
        State("NM" , "New Mexico"),
        State("NV" , "Nevada"),
        State("NY" , "New York"),
        State("OH" , "Ohio"),
        State("OK" , "Oklahoma"),
        State("OR" , "Oregon"),
        State("PA" , "Pennsylvania"),
        State("PR" , "Puerto Rico"),
        State("RI" , "Rhode Island"),
        State("SC" , "South Carolina"),
        State("SD" , "South Dakota"),
        State("TN" , "Tennessee"),
        State("TX" , "Texas"),
        State("UT" , "Utah"),
        State("VA" , "Virginia"),
        State("VI" , "Virgin Islands"),
        State("VT" , "Vermont"),
        State("WA" , "Washington"),
        State("WI" , "Wisconsin"),
        State("WV" , "West Virginia"),
        State("WY" , "Wyoming")]
    }
}
