//
//  State.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/11/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation
struct State {
    let abbreviation: String
    let name: String
    
    var displayable : String {
        return "\(abbreviation) - \(name)"
    }
    init(_ abbreviation: String, _ name: String) {
        self.abbreviation = abbreviation
        self.name = name
    }
}




