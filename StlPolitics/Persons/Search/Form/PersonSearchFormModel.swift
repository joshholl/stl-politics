import Foundation

class PersonSearchFormModel {
    var name: String?
    var city: String?
    var state: String?
    var employer: String?
       
    func canSearch() -> Bool {
        return [name, city, state, employer].contains{ element in
            return element.isNotNilOrEmpty
        }
    }
}
