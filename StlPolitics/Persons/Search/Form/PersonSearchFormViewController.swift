import UIKit

class PersonSearchFormViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var employerTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    let picker = UIPickerView()
    let model = PersonSearchFormModel()
    
    override func viewDidLoad() {
        nameTextField.delegate = self
        cityTextField.delegate = self
        stateTextField.delegate = self
        employerTextField.delegate = self
        searchButton.isEnabled = model.canSearch()
        picker.delegate = self
        stateTextField.inputView = picker
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? PersonSearchResultsViewController else { return }
        
        controller.setCriteria(name: model.name, city: model.city, state: model.state, employer: model.employer)
    }
}

extension PersonSearchFormViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newValue = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        switch (textField) {
        case nameTextField:
            model.name = newValue
        case cityTextField:
            model.city = newValue
        case stateTextField:
            model.state = newValue
        case employerTextField:
            model.employer = newValue
        default:
            break
        }
        searchButton.isEnabled = model.canSearch()
        
        return true
    }
}

extension PersonSearchFormViewController : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return States.all.count
    }
}

extension PersonSearchFormViewController : UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return States.all[row].displayable
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        model.state = States.all[row].abbreviation
        stateTextField.text = model.state
        self.view.endEditing(true)
    }
}
