import Foundation

struct Person: Codable, Equatable {
    let fullName: String
    let state: String
    let city: String
    let company: String
    let position: String
    let id: UUID
    
    static func from(data: CDPerson) -> Person {
        return Person(fullName: data.fullName, state: data.state,city: data.city, company: data.company, position: data.position, id: data.id)
    }
    
    static func from(contributor: IndividualContributor) -> Person {
        return Person(fullName: contributor.contributorName ,state: contributor.contributorState, city: contributor.city, company: contributor.contributorEmployer, position: "", id: UUID() )
    }
    
    static func == (lhs: Person, rhs: Person) -> Bool {
        return lhs.fullName == rhs.fullName &&
            lhs.state == rhs.state &&
            lhs.city == lhs.city &&
        lhs.company == lhs.company

    }
}
