import Foundation
import UIKit

class ContributionDetailViewController : UIViewController {
    
    var donation: Donation!
    
    @IBOutlet weak var comitteeNameLabel: UILabel!
    
    @IBOutlet weak var treasurerLabel: UILabel!
    @IBOutlet weak var committeeTypeLabel: UILabel!
    @IBOutlet weak var partyAffiliationLabel: UILabel!
    
    
    @IBOutlet weak var amountRaisedLabel: UILabel!
    @IBOutlet weak var amountSpentLabel: UILabel!
    @IBOutlet weak var FECLinkButton: UIButton!
    
    
    func setModel(donation: Donation) {
        self.donation = donation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.comitteeNameLabel.text = donation.committee.name
        self.committeeTypeLabel.text =
            donation.committee.committeeTypeFull
        self.partyAffiliationLabel.text = donation.committee.party ?? "Unaffiliated"
        
        self.treasurerLabel.text = donation.committee.treasurerName
    }
    @IBAction func onFECLinkClicked(_ sender: Any) {
        if let url = URL(string: donation.link) {
            UIApplication.shared.open(url)
            
            
        }
        
    }
}
