//
//  SearchCriteriaHeaderView.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/11/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation
import UIKit

class ContributionListHeaderView: UIView {
    
    let person: Person
    let total: Double
    init(rect: CGRect, person: Person, total: Double) {
        self.person = person
        self.total = total
        super.init(frame: rect)
        self.backgroundColor = UIColor.darkGray.withAlphaComponent(CGFloat(0.05))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        let nameLabel = UILabel()
        nameLabel.text = person.fullName;
        nameLabel.textColor = UIColor.black
        nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        nameLabel.frame = CGRect(x: 10, y: 10, width: frame.width, height: 20)
        
        self.addSubview(nameLabel)
        
        let employerLabel = UILabel()
        employerLabel.text = person.company;
        employerLabel.textColor = UIColor.black
        employerLabel.font = UIFont.systemFont(ofSize: 16)
        employerLabel.frame = CGRect(x: 10, y: 30, width: frame.width, height: 20)
        self.addSubview(employerLabel)
        
        
        let cityStateLabel = UILabel()
        cityStateLabel.text = "\(person.city), \(person.state)"
        cityStateLabel.textColor = UIColor.black
        cityStateLabel.font = UIFont.systemFont(ofSize: 16)
        cityStateLabel.frame = CGRect(x: 10, y: 50, width: frame.width, height: 20)
        self.addSubview(cityStateLabel)
        
        let totalLabel = UILabel()
        totalLabel.text = "Total Contributions: \(total.asCurrency())"
        totalLabel.textColor = UIColor.black
        totalLabel.font = UIFont.systemFont(ofSize: 16)
        totalLabel.frame = CGRect(x: 10, y: 70, width: frame.width, height: 20)
        self.addSubview(totalLabel)
    }
}
