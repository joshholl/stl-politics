import Foundation
import UIKit

class ContributionListViewController: UIViewController {
    
    var model: ContributionListModel?
    var spinner = SpinnerUtility()
    
    @IBOutlet weak var contributionsTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.contributionsTableView.dataSource = self
        self.contributionsTableView.delegate = self
        
        spinner.showActivityIndicator(uiView: self.view)
        guard let m = model else { return }
        m.loadData(delegate: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let target = segue.destination as? ContributionDetailViewController {
            guard let cell = sender as? UITableViewCell else { return }
            guard let path = self.contributionsTableView.indexPath(for: cell) else { return }
            guard let contribution = model?.committees[path.row] else { return  }

            target.setModel(donation: contribution)
        }
    }
    
    func setupForPerson(person: Person) {
           model = ContributionListModel(person: person)
    }
}

extension ContributionListViewController: UITableViewDelegate {
    //empty but if this isn't here we cant use viewForHeaderInSection
}


extension ContributionListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: CGFloat(model?.calculateHeightForHeader() ?? 0))
        
        
        return ContributionListHeaderView(rect: headerFrame, person: model!.person, total: model!.total )
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
             return CGFloat(model?.calculateHeightForHeader() ?? 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.committees.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContributionCell", for: indexPath)
        guard let contributions = model?.committees else { return cell }
        
        
        let contribution =  contributions[indexPath.row]
        
        cell.detailTextLabel?.text = contribution.amount.asCurrency()
        cell.textLabel?.text = contribution.committee.name
        
        return cell
    }
    
    
      
     

    

}

extension ContributionListViewController: DataChangedDelegate {
    func onDataChanged() {
        DispatchQueue.main.async { [weak self] in
            guard let s = self else { return }
            s.contributionsTableView.reloadData()
            s.spinner.hideActivityIndicator(uiView: s.view)
        }
    }
}
