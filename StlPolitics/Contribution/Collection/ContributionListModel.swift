//
//  ContributionListViewModel.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/8/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation

class ContributionListModel {
    let session = AppSession.instance
    
    var person: Person
    init(person: Person) {
        self.person = person
    }
    
    var committees: [Donation] = []
    
    var total : Double {
        committees.reduce(0.0) { result, donation in
            return result + donation.amount
        }
    }
    
    func loadData(delegate: DataChangedDelegate) {
        session.api.searchForBusinessPerson(name: person.fullName, state: person.state, city: person.city, employer: person.company) { (result) in
            switch result {
            case .success(let data):
                self.committees = data.filter{$0.committee != nil }
                    .map{ Donation(committee: $0.committee!, amount: $0.contributionReceiptAmount) }
                delegate.onDataChanged()
            case .failure(let error): print(error)
            }
        }
    }
    
    func calculateHeightForHeader() -> Int {
        return 90
    }
}
