import Foundation

struct Donation {
    let committee: Committee
    let amount: Double
    var link: String {
        return "https://www.fec.gov/data/committee/\(committee.id)/"
    }
}
