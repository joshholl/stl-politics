import Foundation

struct PagedResults: Codable{
    var apiVersion: String
    var pagination: SeekInfo
    var results: [IndividualContributor]
    
    enum CodingKeys: String, CodingKey {
        case apiVersion = "api_version"
        case pagination
        case results
    }
}
