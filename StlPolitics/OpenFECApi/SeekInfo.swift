import Foundation

struct LastIndex: Codable {
    var lastIndex: String
    var lastContributionReceiptDate: String
    
    enum CodingKeys: String, CodingKey {
        case lastIndex = "last_index"
        case lastContributionReceiptDate = "last_contribution_receipt_date"
    }
}

struct SeekInfo: Codable {
    var count: Int;
    var lastIndexes: LastIndex?;
    var pages: Int;
    var resultsPerPage: Int;
    
    enum CodingKeys: String, CodingKey {
        case count
        case lastIndexes = "last_indexes"
        case pages
        case resultsPerPage = "per_page"
    }
}
