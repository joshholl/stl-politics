//
//  OpenFECService.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/7/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation


typealias SearchResult = Result<[IndividualContributor], ServiceCallError>

typealias Parameters = [String: String]

class OpenFECService {
    
    let baseUrl: String
    let apiKey: String
    let serviceClient: BaseServiceClient
    
    init(baseUrl: String, apiKey: String) {
        self.baseUrl = baseUrl
        self.apiKey = apiKey
        self.serviceClient = BaseServiceClient()
    }
    
    
    func searchForBusinessPerson(name: String?, state: String?, city: String?, employer: String?, completion: @escaping (SearchResult) -> ()) {
        
        let apiPath = "/v1/schedules/schedule_a"
        
        var queryParameters = [
            "api_key": apiKey,
            "per_page": "20",
            "is_individual":"true",
            "sort_hide_null": "false",
            "sort_null_only":"false",
            "min_date":"01/01/2019",
            "sort":"contribution_receipt_date",
        ]
        if name.isNotNilOrEmpty {
            queryParameters["contributor_name"] = name
        }
        
        if state.isNotNilOrEmpty {
            queryParameters["contributor_state"] = state
        }
        
        if city.isNotNilOrEmpty{
            queryParameters["contributor_city"] = city
        }
        
        if employer.isNotNilOrEmpty {
            queryParameters["contributor_employer"] = employer
        }
        
        
        self.serviceClient.get(from: buildUrl(path: apiPath, params: queryParameters)) { result in
            switch result {
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    
                    let page = try decoder.decode(PagedResults.self, from: data)
                    print(page.results)
                    return completion(.success(page.results))
                } catch let error {
                    completion(.failure(ServiceCallError(message: "Failed to parse json" + error.localizedDescription, code: nil)))
                }
            case .failure(let error):  completion(.failure(error))
            }
        }
    }
    
    private func buildUrl(path: String, params: Parameters) -> URL {
        var components = URLComponents()
        components.queryItems = params.map{ URLQueryItem(name: $0, value: String(describing: $1))}
        components.host = self.baseUrl
        components.scheme = "https"
        components.path = path
        return components.url! //bad but if this fails to build a real url the app cannot work anyways
    }
}

