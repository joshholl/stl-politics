import Foundation

struct Committee: Codable {
    var treasurerName: String
    var party: String?
    var name: String
    var committeeTypeFull: String
    var id: String
 
    
    enum CodingKeys: String, CodingKey {
        case treasurerName = "treasurer_name"
        case party = "party_full"
        case committeeTypeFull = "committee_type_full"
        case name
        case id = "committee_id"
    }
}
