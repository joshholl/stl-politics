//
//  IndividualContributor.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/5/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation

struct IndividualContributor: Codable {
    let amendmentIndicator: String
    let contributionReceiptAmount: Double
    let city: String
    let committeeName: String?
    let committee: Committee?
    let contributorEmployer: String
    let contributorName: String
    
    let contributorState: String
    
    
    enum CodingKeys: String, CodingKey {
        
        case amendmentIndicator = "amendment_indicator"
        case contributionReceiptAmount = "contribution_receipt_amount"
        case city = "contributor_city"
        case committeeName  = "committee_name"
        case committee
        case contributorEmployer = "contributor_employer"
        case contributorName = "contributor_name"
        case contributorState = "contributor_state"
    }
}
