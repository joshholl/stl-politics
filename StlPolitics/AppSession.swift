import Foundation

class AppSession {
    let persistence: StlPoliticsPersistence
    let api: OpenFECService
    
    // ideally the api key wouldn't be hard coded here
    // but since the open fec api only takes it in the get request, and is easily viewable in
    // the browser when using the official site, api key security isn't the biggest concern
    static let instance = AppSession(api: OpenFECService(baseUrl: "api.open.fec.gov",
                                                         apiKey: "2js4Dsn6OOyYTxbPdOuV90Z6b4vAhqUrirVcddsv"),
                                     persistence:StlPoliticsPersistence(container: DbManager().container)
    )
    
    init(api: OpenFECService, persistence: StlPoliticsPersistence) {
        self.api = api
        self.persistence = persistence
    }
}
