//
//  DataChanged.swift
//  StlPolitics
//
//  Created by Josh Hollandsworth on 12/8/19.
//  Copyright © 2019 Joshholl. All rights reserved.
//

import Foundation

protocol DataChangedDelegate {
    func onDataChanged()
}
